var IntlMixin       = ReactIntl.IntlMixin;
var FormattedNumber = ReactIntl.FormattedNumber;
var IntlProvider = ReactIntl.IntlProvider;

Product = React.createClass({
	mixins: [IntlMixin],
	add: function(){
		if(isNaN(this.refs.quantity.value)){
			alert("Quantity is not a valid number.")
		}
		else if(this.refs.quantity.value != ''){
			this.props.addToCart(this.props.id, this.refs.quantity.value);
			this.refs.quantity.value = ''
		}
		else{
			alert('Please fill up quantity field.')
		}
	},
	render: function(){
		return(
		<IntlProvider locale="en">
			<tr>
				<td>{this.props.children}</td>
				<td><FormattedNumber value={this.props.price} style='currency' currency='USD'/></td>
				<td><input type='text' ref='quantity' className='form-control' placeholder='Quantity'/></td>
				<td><input type='button' value='Add' className='btn btn-primary' onClick={this.add}/></td>
			</tr>
		</IntlProvider>
		)
	}
});