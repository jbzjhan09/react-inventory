var InventoryForm = React.createClass({
	add: function(){
		if(this.refs.name.value != '' && this.refs.price.value != '' && !isNaN(this.refs.price.value)){
		var inventoryObj = {}
		inventoryObj.name = this.refs.name.value
		inventoryObj.price = this.refs.price.value
			$.ajax({
				url: '/add',
				type: 'POST',
				data: JSON.stringify(inventoryObj, null, '\t'),
    			contentType: 'application/json;charset=UTF-8',
				success: function(reply){
					alert(reply)
					this.refs.price.value = ''
					this.refs.name.value = ''
				}.bind(this)
			});
		}
		else if(isNaN(this.refs.price.value)){
			alert('Price should be a number.')
		}
		else{
			alert('Please fill up the form correctly.')
		}
	},
	render: function(){
		return(
		<div className='container-fluid'>
			<h1>Add Product</h1>
			<div className='row'>
				<div className='form-group'>
					<label className='control-label col-md-2' for='txtName'>Product name</label>
					<div className='col-md-4'>
						<input type='text' className='form-control' ref='name' name='txtName'/>
					</div>
				</div>
			</div>
			<div className='row'>
				<div className='form-group'>
					<label className='control-label col-md-2' for='txtPrice'>Price</label>
					<div className='col-md-4'>
						<input type='text' className='form-control' ref='price' name='txtPrice' />
					</div>
				</div>
			</div>
			<div className='row'>
				<input type='button' className='btn btn-primary' value='Submit' onClick={this.add}/>
			</div>
		</div>
		)
	}
});
ReactDOM.render(<InventoryForm/>, document.getElementById('addInventory'))