from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from datetime import datetime
from bson import json_util, ObjectId
import json

inventory_blueprint = Blueprint('inventory', __name__)

def toJSON(data):
	return json.dumps(data, default=json_util.default)

@inventory_blueprint.route('/inventory', methods=['POST'])
def product_list():
	inventory = g.inventorydb.getInventory()
	return toJSON([i for i in inventory])

@inventory_blueprint.route('/inventory', methods=['GET'])
def goToInventory():
	return render_template('/inventory/inventory.html')

@inventory_blueprint.route('/add', methods=['GET', 'POST'])
def add_product():
	if request.method == 'POST':
		name = request.json['name']
		price = float(request.json['price'])
		g.inventorydb.insertInventory(name, price)
		return 'Product added successfully.'
	return render_template('/inventory/add_inventory.html')